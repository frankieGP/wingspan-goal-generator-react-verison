import React from 'react'
import { render, screen } from '@testing-library/react'
import { Header } from './Header/Header'

test('renders learn react link', () => {
  render(<Header />)
  const headerComponentText = screen.getByText(/Wingspan/i)
  expect(headerComponentText).toBeInTheDocument()
})
