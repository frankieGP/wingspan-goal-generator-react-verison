import styled from '@emotion/styled';

export const H1 = styled.h1({
  margin: 0,
  fontFamily: 'Roboto',
})

export const H2 = styled.h2({
  margin: 0,
  fontFamily: 'Roboto',
})

export const H3 = styled.h3({
  margin: 0,
  fontFamily: 'Roboto',
})