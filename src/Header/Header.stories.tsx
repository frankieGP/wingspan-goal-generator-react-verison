import React from 'react'
import { Header } from './Header'

export default {
  title: 'Header',
};

export function base () {
  return (
    <Header />
  );
}
