import styled from '@emotion/styled'
import { black, white } from '../lib/colors'

export const HeaderContainer = styled.div({
  width: '100%',
  padding: '16px 0 16px 16px',
  marginBottom: 24,
  backgroundColor: black,
  color: white,

  '@media (min-width: 768px)': {
    paddingLeft: '8%'
  }

})
