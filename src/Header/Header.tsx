import React, { ReactElement } from 'react';
import { H1, H3 } from '../lib/typography.styled'
import {
  HeaderContainer,
} from './Header.styled';


export function Header () :ReactElement {
  return (
    <HeaderContainer>
      <H1>Wingspan</H1>
      <H3>End of Round Goals Generator</H3>
    </HeaderContainer>
  );
}

